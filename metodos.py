import os
import sys


def biseccion(Eq, A1, B1):
    # Recopila los datos necesarios (dataset) para iniciar con la iteracion

    #Eq = input("Que funcion de x vamos a resolver?: ")  # Ingresamos la ecuacion a evaluar
    #A1 = input("Ingresa el rango menor: ")              # refiere al numero de menor valor absoluto del rango solucion
    #B1 = input("Ingresa el rango mayor: ")              # refiere al numero de mayor valor absoluto del intervalo solucion

    if 0 == float(A1) + float(B1):
        B1 = input("Selecciona un nuevo valor para el rango mayor: ")
        # para funcionamiento de la aplicacion evaluamos que el resultado del intervalo no sea 0

    Med = (float(A1) + float(B1) / 2) # punto medio del intervalo

    print("""
                 *************************************
                 * Ecuacion : {Eq}        *
                 * Rango 1:   {A1}                      *
                 * Rango 2:   {B1}                     *
                 * Media :    {Med}                    *
                 *************************************
                """.format(Eq=Eq, A1=A1, B1=B1, Med=Med))
    os.system('pause')
    # visualizamos los datos iniciales

    # Inicia Iteracion

    count = 0   # Contador para el numero de Iteraciones


    fm = 1    # inicio del bucle

    resultados = []

    while float(fm) != 0:

        # Argumentos

        Med = ((float(A1) + float(B1)) / 2)

        x = float(Med)
        fm = (eval(Eq))

        x = float(A1)
        f_A1 = (eval(Eq))

        x = float(B1)
        f_B1 = (eval(Eq))
        # Fin de los argumentos

        # Resultados
        print(A1, "\t\t", B1, "\t\t", Med, "\t\t", f_A1, "\t\t", f_B1, "\t\t", fm)
        fila = str(A1) + "," + str(B1) + "," + str(Med) + "," + str(f_A1) + "," + str(f_B1) + "," + str(fm)

        resultados.append(fila)

        count = count + 1


        # Cambio de  Valores para la siguiente iteracion
        if 0 > (float(f_B1 * fm)):
            A1 = Med
        else:
            A1
        if 0 > (float(f_A1 * fm)):
            B1 = Med
        else:
            B1
            print("\n" "Tomo ", count, "Iteraciones encontrar la raiz de X ", Med)

        return resultados
    # Resultado Final


def crearTablaHtml(resultados):
    tag_tabla = "<table>"
    tag_close_tabla = "</table>"
    tag_fila = "<tr>"
    tag_close_fila = "</tr>"
    tag_columna = "<th>"
    tag_close_columna = "</th>"

    html_columnas = ""
    for i in resultados:
        html_columnas = tag_columna + str(i) + tag_close_columna

    return tag_tabla + html_columnas + tag_close_columna



